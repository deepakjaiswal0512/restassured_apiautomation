package Recursion;

public class powerUsingRecursion {
	
	public static int power(int n,int m) {
		if(m==0) {
			return 1;
		}
		else {
			return power(n,m-1)*n;
		}
	}
	
	public static void main(String[] args) {
		int x = power(2,9);
		System.out.println(x);
				
	}

}
