package Recursion;

public class towerOfHonoiUsingRecursion {
	
	/*public static void toh(int n, int a, int b, int c) {
		if(n>0) {
			toh((n-1),a, c, b);
			System.out.println("Move " +a +" to "+c);
			toh((n-1),b, a, c);
		}
	}
	
	public static void main(String[] args) {
		toh(10,1,2,3);
	}*/
	static int fun1(int x, int y)
	{ 
		  if(x == 0) 
		    return y; 
		  else
		    return fun1(x - 1,  x + y); 
		}
	public static void main(String[] args) {
		int x =fun1(5,2);
		System.out.println(x);
	}

}
