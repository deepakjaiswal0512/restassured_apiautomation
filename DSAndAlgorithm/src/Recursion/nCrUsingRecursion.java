package Recursion;

public class nCrUsingRecursion {

	public static int fact(int n) {
		if (n == 0) {
			return 1;
		} else {
			return fact(n - 1) * n;
		}
	}

	public static int nCr(int n, int r) {
		int neum;
		int deno;
		neum = fact(n);
		deno = fact(r) * fact(n - r);
		return neum / deno;
	}
	
	public static int NCR (int n ,int r) {
		if(r==0||(n==r)) {
			return 1;
		}
		else {
			return NCR((n-1),(r-1)+NCR((n-1),r));
		}
	}
	
	public static void main(String[] args) {
		System.out.println(nCr(5,1));
	}

}
