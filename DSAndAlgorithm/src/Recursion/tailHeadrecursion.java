package Recursion;

public class tailHeadrecursion {
//=============Example of Tail Recursion=========================//
	/*public static void func(int n) {
		
		if(n>0) {
			System.out.println("Print the values of n : "+n);
			func(n-1);
		}
		
		
	}
	
	public static void main(String[] args) {
		
		int x = 3;
		func(x);

	}*/
//============End Example of Tail Recursion=======================//
//============Start Example of Head Recursion ====================//
	public static void func(int n) {

		if (n > 0) {
			func(n - 1);
			System.out.println("Print the values of n : " + n);

		}

	}

	public static void main(String[] args) {
		int x= 3;
		func(x);
				

	}
//============End Example of Tail Recursion=======================//
}


