package Recursion;

public class staticAndGlobalinRecursion {
	static int n = 0; 
	public static int func(int x) {
		
		if(x>0) {
			
			n++;
			return func(x-1) + n;
		}
		return 0;
	}
	
	public static void main(String[] args) {
		int a = 5;
		a =func(5);
		System.out.println("Print the value of a "+a);
		
	}
	

}
