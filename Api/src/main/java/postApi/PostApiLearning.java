package postApi;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class PostApiLearning {

	public static void main(String[] args) {
		
		RequestSpecification reqSpec =RestAssured.given();
		reqSpec.auth().preemptive().basic("ThirdPartyUser", "Password@123");
		reqSpec.log().all();
		reqSpec.baseUri("http://172.19.6.9:2243/api/");
		reqSpec.basePath("CheckFreePay/MakePayment");
		
		reqSpec.body("{\r\n" + 
				"\"transactionTimestamp\":\"2021-06-08 10:22:36.584\",\r\n" + 
				"\"requestId\":\"CR189898SD\",\r\n" + 
				"\"accountNumber\":\"789654\",\r\n" + 
				"\"clientId\":47,\r\n" + 
				"\"divisionId\":47,\r\n" + 
				"\"paymentAmount\":2,\r\n" + 
				"\"tenderType\":\"Cash\"   \r\n" + 
				"}");
		reqSpec.contentType(ContentType.JSON);

		Response resp = reqSpec.request().post();
		ValidatableResponse var = resp.then().log().all();
		var.statusCode(200);
		
	}

}
