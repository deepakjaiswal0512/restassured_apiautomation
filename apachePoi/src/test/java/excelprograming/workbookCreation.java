package excelprograming;

import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class workbookCreation {
	public static void main(String[] args) {
		
		Workbook wb = new HSSFWorkbook();
		//Workbook wbx = new XSSFWorkbook();
		
		try {OutputStream fileout = new FileOutputStream("workbook.xlsx");
		wb.write(fileout);
		System.out.println("Workbook Created Successfully");
			
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

}
